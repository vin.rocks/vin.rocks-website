import React from "react";
import { motion } from "framer-motion";
import Sidebar from "../components/Sidebar";

function ContactRoute() {
  React.useEffect(() => {
    document.title = "Vin H - Contact and more";
  }, []);
  return (
    <div className="flex gap-10">
      <Sidebar />
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.3 }}
        className="py-20 pr-10 ml-0 md:ml-[240px]"
      >
        <div className="flex flex-col flex-1 overflow-y-auto overflow-x-hidden max-w-3xl">
          <div className="flex flex-col flex-1">
            <h1 className="text-3xl font-bold mb-5 text-gray-100">Contact me</h1>
            <p className="text-lg font-normal mb-3 text-gray-300">
              There are alot of ways to contact me. The fastest way is to send me a message on Telegram via <a className="custom-purple-color" href="https://t.me/vindeveloper">t.me/vindeveloper</a>. You can also contact me via E-Mail: <a className="custom-purple-color" href="mailto:vinh260607@gmail.com">vinh260607@gmail.com</a>.
            </p>
            <h1 className="text-3xl mt-10 font-bold mb-5 text-gray-100">More</h1>
            <p className="text-lg font-normal mb-3 text-gray-300">
                You can see all my open source projects on <a className="custom-purple-color" href="https://gitlab.com/vin.rocks">Gitlab</a> including the source code of this website.
            </p>
            <p className="text-lg font-normal mb-3 text-gray-300">
                I will make a personal blog soon where I will post about my projects and other stuff.
            </p>
            <p className="text-lg font-normal mb-3 text-gray-300">
                Thank you so much for visiting my website. I hope you enjoyed it.
            </p> 
          </div>
        </div>
      </motion.div>
    </div>
  );
}

export default ContactRoute;
