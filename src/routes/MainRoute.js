import React from "react";
import { Link } from "react-router-dom";
import { motion } from "framer-motion";
import Sidebar from "../components/Sidebar";

function MainRoute() {
  React.useEffect(() => {
    document.title = "Vin H - Who am I";
  }, []);
  return (
    <div className="flex gap-10">
      <Sidebar />
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.3 }}
        className="py-20 pr-10 ml-0 md:ml-[240px]"
      >
        <div className="flex flex-col flex-1 overflow-y-auto overflow-x-hidden max-w-3xl">
          <div className="flex flex-col flex-1">
            <h1 className="text-3xl font-bold mb-5 text-gray-100">Who am I</h1>
            <p className="text-lg font-normal mb-3 text-gray-300">
              My name is Vin and I am a 15 year old fullstack developer from
              Germany.
            </p>
            <p className="text-lg font-normal mb-3 text-gray-300">
              I started programming when I was 12 and I have been learning ever
              since. Designing, building and publishing products of quality and
              reliability is something I do as a hobby.
            </p>
            <p className="text-lg font-normal mb-3 text-gray-300">
              I specialise in web development and I am currently working on a
              few projects including{" "}
              <a href="https://rolink.pro" className="custom-purple-color">
                Rolink
              </a>
              .
            </p>
            <p className="text-lg font-normal mb-3 text-gray-300">
              If you want to contact me, you can do so by visiting my{" "}
              <Link to="/contact" className="custom-purple-color">
                contact page
              </Link>
              .
            </p>
            <h1 className="text-3xl font-bold mb-5 text-gray-100 mt-10">
              What is Rolink
            </h1>
            <p className="text-lg font-normal mb-3 text-gray-300">
              Rolink is a Guilded bot that connects each user to their Roblox
              account, allowing them to get certain roles or permissions in a
              Guilded server. Users can verify their identity by joining a game
              or pasting a phrase in their bio.
            </p>
          </div>
        </div>
      </motion.div>
    </div>
  );
}

export default MainRoute;
