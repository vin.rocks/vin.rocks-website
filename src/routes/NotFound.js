import React from "react";
import { motion } from "framer-motion";
import Sidebar from "../components/Sidebar";

function NotFound() {
  React.useEffect(() => {
    document.title = "Vin H - 404";
  }, []);
  return (
    <div className="flex gap-10">
      <Sidebar />
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.3 }}
        className="py-20 pr-10 ml-0 md:ml-[240px]"
      >
        <div className="flex flex-col flex-1 overflow-y-auto overflow-x-hidden max-w-3xl">
          <div className="flex flex-col flex-1">
            <h1 className="text-3xl font-bold mb-5 text-gray-100">404 - Not Found!</h1>
            <p className="text-lg font-normal mb-3 text-gray-300">
              Sorry pal it seems like you've stumbled upon a page that doesn't exist. You can go back to the another page by clicking on any button on the left sidebar. If you think this is a bug, please contact me via E-Mail: <a className="custom-purple-color" href="mailto:hi@vin.rocks">hi@vin.rocks</a>
            </p>
            
          </div>
        </div>
      </motion.div>
    </div>
  );
}

export default NotFound;
