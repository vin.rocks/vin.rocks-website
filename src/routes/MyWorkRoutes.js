import React from "react";
import { motion } from "framer-motion";
import Sidebar from "../components/Sidebar";
import { FiExternalLink } from "react-icons/fi";
const bigProjectArray = [
  {
    name: "Rolink",
    link: "https://rolink.pro",
    img: "./img/rolink-logo.png",
    description: "Verification Bot",
    role: "Internal / Security / Website",
  },
]

const projectArray = [
  {
    name: "vin.rocks",
    link: "https://gitlab.com/vin.rocks/vin.rocks-website",
    language: "JavaScript",
    description: "My personal portfolio website made with ReactJS",
    dotColor: "#f0e035"
  },
  {
    name: "lua_localizer",
    link: "https://gitlab.com/vin.rocks/lua_localizer",
    language: "CSharp",
    description: "Lua localizer is a program written in C# which has been made to localize Lua 5.1 code.",
    dotColor: "#8b00ff"
  },
]

function MyWorkRoute() {
  React.useEffect(() => {
    document.title = "Vin H - My work";
  }, []);
  return (
    <div className="flex gap-10">
      <Sidebar />
      <motion.div
        initial={{ opacity: 0 }}
        animate={{ opacity: 1 }}
        transition={{ duration: 0.3 }}
        className="py-20 w-full pr-10 ml-0 md:ml-[240px]"
      >
        <div className="flex flex-col w-full flex-1 overflow-y-auto overflow-x-hidden">
          <div className="flex flex-col flex-1">
            <h1 className="text-3xl font-bold mb-5 text-gray-100">My work</h1>
            <h2 className="text-xl font-normal mb-5 text-gray-100">Big Projects (Closed Source)</h2>
            <div className="flex flex-row flex-wrap gap-5">
              {bigProjectArray.map((item, index) => (
                <a href={item.link} key={index} className="flex-1 big-proj">
                  <div className="border rounded-xl">
                    <div className="flex flex-row border-bottom  py-10 px-5">
                      <img alt={`${item.name} image`} src={item.img} className="w-20 h-20 rounded-xl" />
                      <div className="flex flex-col justify-center ml-5">
                        <h1 className="text-lg font-bold text-gray-100 flex flex-row gap-2 place-items-center">{item.name} <FiExternalLink /></h1>
                        <h2 className="text-lg font-normal text-gray-100">{item.description}</h2>
                      </div>
                    </div>
                    <div>
                      <div className="p-7">
                        <h1 className="text-xl font-semibold mb-5 text-white">My Role</h1>
                        <h2 className="text-lg font-normal text-gray-100">{item.role}</h2>
                      </div>
                    </div>
                  </div>
                </a>
              ))}
            </div>
            <h2 className="text-xl font-normal mb-5 text-gray-100 mt-10">Open Source Projects</h2>
            <div className="flex flex-col">
              {projectArray.map((item, index) => (
                <a href={item.link} className="mb-5">
                  <div className="flex max-h-42 flex-col md:flex-row border rounded-xl">
                    <div className="flex flex-col border-bottom-proj border-right py-10 px-5">
                      <h5 className="text-md font-thin text-gray-200"><code>name</code></h5>
                      <div className="flex flex-row w-28">
                        <h1 className="text-lg font-normal text-gray-100 mr-2">{item.name}</h1>
                      </div>
                    </div>
                    <div className="flex flex-col border-bottom-proj border-right py-10 px-5">
                      <h5 className="text-md font-thin text-gray-200"><code>language</code></h5>
                      <div className="flex flex-row w-28 items-center">
                        <h1 className="text-lg font-normal text-gray-100 mr-2">{item.language}</h1>
                        <div className="w-3 h-3 rounded-full" style={{ backgroundColor: item.dotColor }}></div>
                      </div>
                    </div>
                    <div className="flex flex-col py-10 px-5">
                      <h5 className="text-md font-thin text-gray-200"><code>description</code></h5>
                      <h1 className="text-lg font-normal text-gray-100">{item.description}</h1>
                    </div>
                  </div>
                </a>
              ))}
            </div>
          </div>
        </div>
      </motion.div>
    </div>
  );
}

export default MyWorkRoute;
